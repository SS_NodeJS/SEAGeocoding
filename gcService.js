'use strict';

//Imports
const Hapi = require('hapi');
const Redis = require('catbox-redis');
const GC = require('./index');
const Fs = require('fs');
const Ini = require('ini');

//Config parsing
let config = Ini.parse(Fs.readFileSync('./config.ini', 'utf-8'));
//Redis cache config
const redisHost = config.Redis.host;
const redisPort = config.Redis.port;
const redisPasswd = config.Redis.passwd;
const expirationTime = parseInt(config.Redis.expires, 10);
const gcTimeout = parseInt(config.Redis.timeout, 10);
//Geocoding Service config
const gcHost = config.Geocoding.host;
const gcPort = config.Geocoding.port;

const startGcServer = async () => {
  //server initialization
  const gcServer = Hapi.server({
    host: gcHost,
    port: gcPort,
    cache: [
      {
        engine: Redis,
        host: redisHost,
        port: redisPort,
        password: redisPasswd,
        partition: 'cache'
      }
    ]
  });
  //Cache functionality
  const gcCache = gcServer.cache({
    expiresIn: expirationTime,
    segment: 'customSegment',
    generateFunc: async (id) => {
      try {
        let result = await GC(id.city);
        return result;
      }
      catch( err ) {
        //console.log('Cache get=>' + err);
        throw err;
      }
    },
    generateTimeout: gcTimeout
  });
  //Routes
  gcServer.route([
    {
      path: '/{city}',
      method: 'GET',
      handler: async function (request, h) {
        try {
          const city = request.params.city;
          const id = `${city}`;
          let response = await gcCache.get({ id, city });
          return response;
        }
        catch(err) {
          let errorResponse = {'error': err.message};
          console.log("Handler:" + err);
          return errorResponse;
        }
      }
    },
    {
        path: '/favicon.ico',
        method: 'GET',
        handler: async function (request, h) {
          return h.response(204);
        }
    }
  ]);

  await gcServer.start();
  console.log('Server running at:', gcServer.info.uri);
};

startGcServer().catch( (err) => {
  console.log(err);
}).then();

module.exports = startGcServer;
